package com.sau;

import com.sau.functions.*;
import com.sau.interfaces.*;
import com.sau.functions.basic.*;
import com.sau.threads.*;

import java.io.*;

public class Main {
    public static void nonThread() {
        Task task = new Task(100);
        for (int i = 0; i < task.getTasks(); i++) {
            int a = 2 + (int)(Math.random() * 9);
            task.setFunction(new Log(a));
            task.setLeftX(Math.random() * 100);
            task.setRightX(Math.random() * 100 + 100);
            task.setStep(Math.random());
            System.out.println("Source leftX = " + task.getLeftX() + " rightX = " + task.getRightX() + " step = " + task.getStep());
            double result = Functions.integral(task.getFunction(), task.getLeftX(), task.getRightX(), task.getStep());
            System.out.println("Result leftX = " + task.getLeftX() + " rightX = " + task.getRightX() + " step = " + task.getStep() + " integrate = " + result);
        }
    }

    public static void simpleThreads() {
        Task task = new Task(100);
        Thread generator = new Thread(new SimpleGenerator(task));
        Thread integrator = new Thread(new SimpleIntegrator(task));

        generator.start();
        integrator.start();
    }

    static void complicatedThreads() throws InterruptedException {
        Task task = new Task(100);
        Semaphore semaphore = new Semaphore();
        Generator generator = new Generator(task, semaphore);
        Integrator integrator = new Integrator(task, semaphore);

        integrator.setPriority(10);

        generator.start();
        integrator.start();
        Thread.sleep(50);
        generator.interrupt();
        integrator.interrupt();

    }

    public static void main(String[] args) throws IOException, FunctionPointIndexOutOfBoundsException, InappropriateFunctionPointException, ClassNotFoundException, CloneNotSupportedException, InterruptedException {
        double rightEnd = Math.PI * 2;
        Sin sin = new Sin();
        Cos cos = new Cos();
        TabulatedFunction tab_sin = TabulatedFunctions.tabulate(sin, 0, Math.PI * 2, 10);
        TabulatedFunction tab_cos = TabulatedFunctions.tabulate(cos, 0, Math.PI * 2, 10);

        for (double i = 0; i <= rightEnd; i += 0.1) {
            System.out.printf("Sin: %f TabSin: %f \t Cos: %f TabCos: %f\n",
                    sin.getFunctionValue(i), tab_sin.getFunctionValue(i),
                    cos.getFunctionValue(i), tab_cos.getFunctionValue(i));
        }

        Function trigonomSumOfSquares = Functions.sum(Functions.power(tab_sin, 2.0), Functions.power(tab_cos, 2.0));

        System.out.println("Sum of squares sine and cos");
        for (double i = 0; i <= rightEnd; i += 0.1) {
            System.out.printf("Arg: %f Value:%f \n", i,  trigonomSumOfSquares.getFunctionValue(i));
        }

        Exp exp = new Exp();
        FileWriter fw = new FileWriter(new File("firstfile"));
        TabulatedFunction tab_exp = TabulatedFunctions.tabulate(exp,0, 10, 11);
        TabulatedFunctions.writeTabulatedFunction(tab_exp, fw);
        fw.close();

        FileReader fr = new FileReader(new File("firstfile"));
        TabulatedFunction read_tab_exp = TabulatedFunctions.readTabulatedFunction(fr);

        System.out.println("Exp");
        for(int i = 0; i <= 10; ++i) {
            System.out.printf("Exp: %f \t Readed tab Exp: %f \n",
                    exp.getFunctionValue(i),
                    read_tab_exp.getFunctionValue(i));
        }

        Log log = new Log(Math.E);
        OutputStream fs = new FileOutputStream("secondile");
        TabulatedFunction tab_log = TabulatedFunctions.tabulate(log, 0, 10, 11);
        TabulatedFunctions.outputTabulatedFunction(tab_log, fs);
        fs.close();
        FileInputStream fi = new FileInputStream(new File("secondile"));
        TabulatedFunction read_tab_log = TabulatedFunctions.inputTabulatedFunction(fi);
        fi.close();

        System.out.println("Log");
        for(int i = 0; i <= 10; ++i) {
            System.out.printf("Log: %f \t Readed tab Log: %f \n",
                    log.getFunctionValue(i),
                    read_tab_log.getFunctionValue(i));
        }

        Function f = Functions.composition(new Log(Math.E), new Exp());
        TabulatedFunction log_exp = TabulatedFunctions.tabulate(f, 0, 10, 11);
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("serialize.dump"));
        oos.writeObject(log_exp);
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("serialize.dump"));
        TabulatedFunction serlz_log_exp = (TabulatedFunction)ois.readObject();
        ois.close();

        System.out.println("Log_exp");
        for(int i = 0; i <= 10; ++i) {
            System.out.printf("Log_exp: %f \t Serialize Log_exp: %f \n",
                    log_exp.getFunctionValue(i),
                    serlz_log_exp.getFunctionValue(i));
        }

        FunctionPoint g = new FunctionPoint(199,452);
        FunctionPoint f2 = new FunctionPoint(199,452);
        FunctionPoint f3 = new FunctionPoint(1,5);
        System.out.println(g.hashCode());
        System.out.println(f2.hashCode());
        //System.out.println(f3 == f3.clone());
        ArrayTabulatedFunction a1 = new ArrayTabulatedFunction(0,100,11);
        ArrayTabulatedFunction a2 = (ArrayTabulatedFunction) a1.clone();
        System.out.println(a1 == a2);
        System.out.println(a2.hashCode());

        Function f4 = new Exp();
        System.out.println(Functions.integral(f4, 0, 1, 0.0005));

        //nonThread();
        //simpleThreads();
        complicatedThreads();

        Thread.sleep(1000);

        for (FunctionPoint p : tab_exp) {
            System.out.println(p);
        }

        Function e = new Cos();
        TabulatedFunction tf;
        tf = TabulatedFunctions.tabulate(e, 0, Math.PI, 11);
        System.out.println(tf.getClass());
        TabulatedFunctions.setTabulatedFunctionFactory(new LinkedListTabulatedFunction.LinkedListTabulatedFunctionFactory());
        tf = TabulatedFunctions.tabulate(e, 0, Math.PI, 11);
        System.out.println(tf.getClass());
        TabulatedFunctions.setTabulatedFunctionFactory(new ArrayTabulatedFunction.ArrayTabulatedFunctionFactory());
        tf = TabulatedFunctions.tabulate(e, 0, Math.PI, 11);
        System.out.println(tf.getClass());


        TabulatedFunction h;
        h = TabulatedFunctions.createTabulatedFunction(
                ArrayTabulatedFunction.class, 0, 10, 3);
        System.out.println(h.getClass());
        System.out.println(h);

        h = TabulatedFunctions.createTabulatedFunction(
                ArrayTabulatedFunction.class, 0, 10, new double[]{0, 10});
        System.out.println(h.getClass());
        System.out.println(h);

        h = TabulatedFunctions.createTabulatedFunction(
                LinkedListTabulatedFunction.class,
                new FunctionPoint[]{
                        new FunctionPoint(0, 0),
                        new FunctionPoint(10, 10)
                }
        );
        System.out.println(h.getClass());
        System.out.println(h);

        h = TabulatedFunctions.tabulate(
                LinkedListTabulatedFunction.class, new Sin(), 0, Math.PI, 11);
        System.out.println(h.getClass());
        System.out.println(h);
    }
}
