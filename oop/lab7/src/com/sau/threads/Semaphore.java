package com.sau.threads;

/**
 * Простейщая реализация механизма семафора
 * canWrite - булева переменная, отвечающая за возможность записи в данный момент
 */
public class Semaphore {

    private boolean canWrite = true;

    /**
     * Метод, вызываемый в начале чтения из объекта потока
     * @throws InterruptedException ex
     */
    public synchronized void beginRead()
            throws InterruptedException {
        while (canWrite) {
            wait(); // ставит поток в ожидание
        }
    }

    /**
     * Метод, вызываемый в конце чтения из объекта потока
     */
    public synchronized void endRead() {
        canWrite = true;
        notifyAll();
    }

    /**
     * Метод, вызываемый в начале записи в объект потока
     * @throws InterruptedException ex
     */
    public synchronized void beginWrite()
            throws InterruptedException {
        while (!canWrite) {
            wait();
        }
    }

    /**
     * Метод, вызываемый в конце записи в объект потока
     */
    public synchronized void endWrite() {
        canWrite = false;
        notifyAll();// пробуждает все потоки
    }
}

