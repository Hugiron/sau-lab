package com.sau.threads;

import com.sau.functions.Functions;

/**
 * Расширенный класс решения задач интегрирования
 * task - объект задачи Task
 * semaphore - простейшая реализация семафора
 * isRun - булева переменная, определяющая, запущен ли поток в данный момент
 */
public class Integrator extends Thread {

    private Task task;
    private Semaphore semaphore;
    private boolean isRun = false;

    /**
     * Параметризированный конструктор
     * @param task - объект задачи Task
     * @param semaphore - объект семафора
     */
    public Integrator(Task task, Semaphore semaphore) {
        this.task = task;
        this.semaphore = semaphore;
    }

    /**
     * Решение задачи по нахождению интеграла в отдельном потоке
     */
    @Override
    public void run() {
        isRun = true;
        for (int i = 0; i < task.getTasks() && isRun; i++) {
            try {
                semaphore.beginRead();
                double result = Functions.integral(task.getFunction(), task.getLeftX(), task.getRightX(), task.getStep());
                System.out.println("Result leftX = " + task.getLeftX() + " rightX = " + task.getRightX() + " step = " + task.getStep() + " integrate = " + result);
                semaphore.endRead();
            } catch (InterruptedException e) {
                System.out.println("Интегратор прервали во время сна, он корректно завершил свою работу");
            }
        }
    }

    @Override
    public void interrupt() {
        super.interrupt();
        isRun = false;
    }

}
