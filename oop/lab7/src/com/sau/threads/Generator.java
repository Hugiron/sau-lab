package com.sau.threads;

import com.sau.functions.basic.Log;

/**
 * Расширенный класс генерации заданий для интегрирования
 * task - объект задачи Task
 * semaphore - простейшая реализация семафора
 * isRun - булева переменная, определяющая, запущен ли поток в данный момент
 */
public class Generator extends Thread {

    private Task task;
    private Semaphore semaphore;
    private boolean isRun = false;

    /**
     * Параметризированный конструктор
     * @param task - объект задачи Task
     * @param semaphore - объект семафора
     */
    public Generator(Task task, Semaphore semaphore) {
        this.task = task;
        this.semaphore = semaphore;
    }

    /**
     * Генерация задачи для решения в отдельном потоке
     */
    @Override
    public void run() {
        isRun = true;
        for (int i = 0; i < task.getTasks() && isRun; i++) {
            try {
                Log log = new Log(2 + (Math.random() * 9));
                semaphore.beginWrite();
                task.setFunction(log);
                task.setLeftX(Math.random() * 100);
                task.setRightX(Math.random() * 100 + 100);
                task.setStep(Math.random());
                semaphore.endWrite();
                System.out.println("Source leftX = " + task.getLeftX() + " rightX = " + task.getRightX() + " step = " + task.getStep());
            } catch (InterruptedException ex) {
                System.out.println("Генератор прервали во время сна, он корректно завершил свою работу");
            }
        }
    }

    @Override
    public void interrupt() {
        super.interrupt();
        isRun = false;
    }

}
