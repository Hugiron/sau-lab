package com.sau.threads;

import com.sau.functions.basic.Log;

/**
 * Простейшая реализация генератора задач Task
 */
public class SimpleGenerator implements Runnable {
    private Task task;

    public SimpleGenerator(Task task) {
        this.task = task;
    }

    @Override
    public void run() {
        synchronized (task) {
            for (int i = 0; i < task.getTasks(); i++) {
                Log log = new Log(1 + (Math.random() * 9));
                task.setFunction(log);
                task.setLeftX(Math.random() * 100);
                task.setRightX(Math.random() * 100 + 100);
                task.setStep(Math.random());
                System.out.println("Source leftX = " + task.getLeftX() + " rightX = " + task.getRightX() + " step = " + task.getStep());

            }
        }
    }
}
