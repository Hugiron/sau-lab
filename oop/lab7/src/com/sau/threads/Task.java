package com.sau.threads;

import com.sau.interfaces.Function;

/**
 * Класс, реализующий задачу.
 * function - функция, подлежащая интегрированию
 * leftX - левая граница интегрирования
 * rightX - правая граница интегрирования
 * step - шаг дескритизации
 * tasks - кол-во задач
 */

public class Task {
    private Function function;// ссылка на объект интегрируемой функции
    private double leftX, rightX, step;
    private int tasks;// количество выполняемых заданий

    public Task(int count) {
        if (count<=0) {
            throw new IllegalArgumentException();
        }
        this.tasks = count;
    }

    public int getTasks(){
        return this.tasks;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public void setLeftX(double leftX) {
        this.leftX = leftX;
    }

    public void setRightX(double rightX) {
        this.rightX = rightX;
    }

    public void setStep(double step) {
        this.step = step;
    }

    public void setTasks(int tasks) {
        this.tasks = tasks;
    }

    public double getLeftX() {
        return leftX;
    }

    public double getRightX() {
        return rightX;
    }

    public double getStep() {
        return step;
    }

    public Function getFunction() {
        return function;
    }
}
