package com.sau.interfaces;

import com.sau.functions.FunctionPoint;

/**
 * Интерфейс, описывающий паттерн фабрики
 */
public interface TabulatedFunctionFactory {
    TabulatedFunction createTabulatedFunction(double leftX, double rigthX, double[] values);
    TabulatedFunction createTabulatedFunction(double leftX, double rigthX, int pointsCount);
    TabulatedFunction createTabulatedFunction(FunctionPoint[] array);
}

