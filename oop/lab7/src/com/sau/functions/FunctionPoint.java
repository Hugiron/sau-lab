package com.sau.functions;

import java.io.Serializable;

public class FunctionPoint implements Comparable<FunctionPoint>, Serializable {
    private double x;
    private double y;

    public FunctionPoint() {
        this.x = 0;
        this.y = 0;
    }

    public FunctionPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public FunctionPoint(FunctionPoint point) {
        this.x = point.getX();
        this.y = point.getY();
    }

    public int compareTo(FunctionPoint point) {
        return Double.compare(this.getX(), point.getX());
    }

    public double getX() {
        return this.x;
    }

    public double setX(double x) {
        return this.x = x;
    }

    public double getY() {
        return this.y;
    }

    public double setY(double y) {
        return this.y = y;
    }

    @Override
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && getClass() == obj.getClass() &&
                ((FunctionPoint)obj).getX() == x && ((FunctionPoint)obj).getY() == y;
    }

    @Override
    public int hashCode() {
        String val1 = String.valueOf(Double.doubleToLongBits(x));
        if (val1.length() > 8)
            val1 = val1.substring(0,8);
        String val2 = String.valueOf(Double.doubleToLongBits(y));
        if (val2.length() > 8)
            val2 = val2.substring(0,8);
        int a = Integer.parseInt(val1);
        int b = Integer.parseInt(val2);
        return (a >> b) ^ (b >> a);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new FunctionPoint(this);
    }
}
