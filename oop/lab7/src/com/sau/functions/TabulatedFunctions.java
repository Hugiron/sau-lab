package com.sau.functions;

import com.sau.interfaces.Function;
import com.sau.interfaces.TabulatedFunction;
import com.sau.interfaces.TabulatedFunctionFactory;

import javax.xml.crypto.NoSuchMechanismException;
import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public final class TabulatedFunctions {
    private static TabulatedFunctionFactory factory = new ArrayTabulatedFunction.ArrayTabulatedFunctionFactory();

    public static void setTabulatedFunctionFactory(TabulatedFunctionFactory factory) {
        TabulatedFunctions.factory = factory;
    }

    public static TabulatedFunction createTabulatedFunction(FunctionPoint[] points) {
        return factory.createTabulatedFunction(points);
    }

    public static TabulatedFunction createTabulatedFunction(double leftX, double rightX, int pointsCount) {
        return factory.createTabulatedFunction(leftX, rightX, pointsCount);
    }

    public static TabulatedFunction createTabulatedFunction(double leftX, double rightX, double[] values) {
        return factory.createTabulatedFunction(leftX, rightX, values);
    }

    public static TabulatedFunction createTabulatedFunction(Class<? extends TabulatedFunction> functionClass, FunctionPoint[] points) {
        Constructor constructors[] = functionClass.getConstructors(); // возвращаем только открытве конструкторы
        for (Constructor constructor : constructors) { // присвоение. Цикл по всему массиву
            Class types[] = constructor.getParameterTypes();
            if (types[0].equals(points.getClass())) {
                try {
                    return (TabulatedFunction) constructor.newInstance(new Object[]{points});// создаем экземпляры исходного класса
                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        throw new NoSuchMechanismException();
    }

    public static TabulatedFunction createTabulatedFunction(Class<? extends TabulatedFunction> functionClass, double leftX, double rightX, int pointsCount) {
        Constructor constructors[] = functionClass.getConstructors();
        for (Constructor constructor : constructors) {
            Class types[] = constructor.getParameterTypes();
            if (types.length == 3 && types[0].equals(Double.TYPE) && types[1].equals(Double.TYPE) && types[2].equals(Integer.TYPE)) {
                try {
                    return (TabulatedFunction) constructor.newInstance(leftX, rightX, pointsCount);
                } catch (IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        throw new NoSuchMechanismException();
    }

    public static TabulatedFunction createTabulatedFunction(Class<? extends TabulatedFunction> functionClass, double leftX, double rightX, double[] values) {
        Constructor constructors[] = functionClass.getConstructors();
        for (Constructor constructor : constructors) {
            Class types[] = constructor.getParameterTypes();
            if (types.length == 3 && types[0].equals(Double.TYPE) && types[1].equals(Double.TYPE) && types[2].equals(values.getClass())) {
                try {
                    return (TabulatedFunction) constructor.newInstance(leftX, rightX, values);
                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        throw new NoSuchMechanismException();
    }

    public static TabulatedFunction tabulate(Class<? extends TabulatedFunction> functionClass, Function function, double leftX, double rightX, int pointsCount) throws IllegalArgumentException {
        if(function.getLeftDomainBorder() > leftX || function.getRightDomainBorder() < rightX ) {
            throw new IllegalArgumentException("Выход за границы табуляции.");
        }
        double step = (rightX - leftX) / (pointsCount - 1);
        double[] points = new double[pointsCount];

        for (int i = 0; i < pointsCount; ++i) {
            points[i] = function.getFunctionValue(leftX + step * i);
        }

        return TabulatedFunctions.createTabulatedFunction(functionClass, leftX, rightX, points);
    }

    public static TabulatedFunction tabulate(Function function, double leftX, double rightX, int pointsCount) throws IllegalArgumentException {
        if(function.getLeftDomainBorder() > leftX || function.getRightDomainBorder() < rightX ) {
            throw new IllegalArgumentException("Выход за границы табуляции.");
        }
        double step = (rightX - leftX) / (pointsCount - 1);
        double[] points = new double[pointsCount];

        for (int i = 0; i < pointsCount; ++i) {
            points[i] = function.getFunctionValue(leftX + step * i);
        }

        return TabulatedFunctions.createTabulatedFunction(leftX, rightX, points);
    }

    public static void outputTabulatedFunction(TabulatedFunction function, OutputStream out) throws IOException {
        DataOutputStream dos = new DataOutputStream(out);
        dos.writeInt(function.getPointsCount());
        for (int i = 0; i < function.getPointsCount(); ++i) {
            dos.writeDouble(function.getPoint(i).getX());
            dos.writeDouble(function.getPoint(i).getY());
        }
    }

    public static TabulatedFunction inputTabulatedFunction(Class<? extends TabulatedFunction> functionClass, InputStream in) throws IOException {
        DataInputStream is = new DataInputStream(in);
        int itemsCount = is.readInt();
        FunctionPoint[] date = new FunctionPoint[itemsCount];
        double x, y;
        for (int i = 0; i < itemsCount; ++i) {
            x = is.readDouble();
            y = is.readDouble();
            date[i] = new FunctionPoint(x, y);
        }

        return TabulatedFunctions.createTabulatedFunction(functionClass, date);
    }

    public static TabulatedFunction inputTabulatedFunction(InputStream in) throws IOException {
        DataInputStream is = new DataInputStream(in);
        int itemsCount = is.readInt();
        FunctionPoint[] date = new FunctionPoint[itemsCount];
        double x, y;
        for (int i = 0; i < itemsCount; ++i) {
            x = is.readDouble();
            y = is.readDouble();
            date[i] = new FunctionPoint(x, y);
        }

        return TabulatedFunctions.createTabulatedFunction(date);
    }

    public static void writeTabulatedFunction(TabulatedFunction function, Writer out) {
        PrintWriter os = new PrintWriter(out);
        os.println(function.getPointsCount());
        for (int i = 0; i < function.getPointsCount(); ++i) {
            os.println(String.valueOf(function.getPoint(i).getX()));
            os.println(String.valueOf(function.getPoint(i).getY()));
        }
    }

    public static TabulatedFunction readTabulatedFunction(Class<? extends TabulatedFunction> functionClass, Reader in) throws IOException {
        StreamTokenizer st = new StreamTokenizer(in);
        int token = st.nextToken();
        int itemsCount = (int)st.nval;
        FunctionPoint[] date = new FunctionPoint[itemsCount];
        double x, y;
        for (int i = 0; i < itemsCount; ++i) {
            st.nextToken();
            x = st.nval;
            st.nextToken();
            y = st.nval;
            date[i] = new FunctionPoint(x, y);
        }

        return TabulatedFunctions.createTabulatedFunction(functionClass, date);
    }

    public static TabulatedFunction readTabulatedFunction(Reader in) throws IOException {
        StreamTokenizer st = new StreamTokenizer(in);
        int token = st.nextToken();
        int itemsCount = (int)st.nval;
        FunctionPoint[] date = new FunctionPoint[itemsCount];
        double x, y;
        for (int i = 0; i < itemsCount; ++i) {
            st.nextToken();
            x = st.nval;
            st.nextToken();
            y = st.nval;
            date[i] = new FunctionPoint(x, y);
        }

        return TabulatedFunctions.createTabulatedFunction(date);
    }
}

