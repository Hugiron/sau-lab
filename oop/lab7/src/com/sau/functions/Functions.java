package com.sau.functions;

import com.sau.functions.meta.*;
import com.sau.interfaces.Function;

public final class Functions {
    public static double integral(Function f, double leftX, double rightX, double step) throws FunctionPointIndexOutOfBoundsException, IllegalArgumentException {
        if (leftX < f.getLeftDomainBorder() || rightX > f.getRightDomainBorder()) {
            throw new FunctionPointIndexOutOfBoundsException("Выход за границы области определения.");
        }
        if (rightX - leftX <= step) {
            throw new IllegalArgumentException("Неверные аргументы");
        }
        double S = 0;
        double d = rightX - leftX;
        double value = leftX;
        while (d > step && d > 0) {
            S += ((f.getFunctionValue(value) + f.getFunctionValue(value + step)) / 2) * step;
            value += step;
            d -= step;
        }
        S += ((f.getFunctionValue(value) + f.getFunctionValue(rightX)) / 2) * (rightX - value);

        return S;
    }

    public static Function shift(Function f, double shiftX, double shiftY) {
        return new Shift(f, shiftX, shiftY);
    }

    public static Function scale(Function f, double scaleX, double scaleY) {
        return new Scale(f, scaleX, scaleY);
    }

    public static Function power(Function f, double power) {
        return new Power(f, power);
    }

    public static Function sum(Function f1, Function f2) {
        return new Sum(f1, f2);
    }

    public static Function mult(Function f1,Function f2) {
        return new Mult(f1, f2);
    }

    public static Function composition(Function f1, Function f2) {
        return new Composition(f1, f2);
    }
}
