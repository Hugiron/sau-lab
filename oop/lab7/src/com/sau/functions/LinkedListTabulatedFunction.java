package com.sau.functions;

import com.sau.interfaces.*;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedListTabulatedFunction implements TabulatedFunction {
    public static class LinkedListTabulatedFunctionFactory implements TabulatedFunctionFactory {
        @Override
        public TabulatedFunction createTabulatedFunction(FunctionPoint[] points) {
            return new LinkedListTabulatedFunction(points);
        }

        @Override
        public TabulatedFunction createTabulatedFunction(double leftX, double rightX, int pointsCount) {
            return new LinkedListTabulatedFunction(leftX, rightX, pointsCount);
        }

        @Override
        public TabulatedFunction createTabulatedFunction(double leftX, double rightX, double[] values) {
            return new LinkedListTabulatedFunction(leftX, rightX, values);
        }
    }

    private FunctionNode root;
    private FunctionNode lastNode;
    private int itemsCount;
    private int lastIndex;

    public LinkedListTabulatedFunction(double leftLimit, double rightLimit, int itemsCount) throws IllegalArgumentException {
        if (leftLimit >= rightLimit || itemsCount < 2) {
            throw new IllegalArgumentException("Были переданы некорректные аргументы.");
        }
        root = new FunctionNode(root, root, null);

        double step = (rightLimit - leftLimit) / (itemsCount - 1);

        for (int i = 0; i < itemsCount; ++i) {
            addNodeToTail().setPoint(new FunctionPoint(leftLimit + step * i, 0.0));
        }
        //this.itemsCount = itemsCount;
    }

    public LinkedListTabulatedFunction(double leftLimit, double rightLimit, double[] values) throws IllegalArgumentException {
        if (leftLimit >= rightLimit || values.length < 2) {
            throw new IllegalArgumentException("Были переданы некорректные аргументы.");
        }
        root = new FunctionNode();
        root.setNext(root);
        root.setPrev(root);
        //this.itemsCount = values.length;
        double step = (rightLimit - leftLimit) / (values.length - 1);

        for (int i = 0; i < values.length; ++i) {
            addNodeToTail().setPoint(new FunctionPoint(leftLimit + step * i, values[i]));
        }
    }

    public LinkedListTabulatedFunction(FunctionPoint[] values) throws IllegalArgumentException {
        if (values.length < 2)
            throw new IllegalArgumentException("Количество точек менее двух");
        for (int i = 1; i < values.length; ++i)
            if (values[i].getX() < values[i - 1].getX())
                throw new IllegalArgumentException("Исходный массив точек не упорядочен");

        root = new FunctionNode();
        root.setNext(root);
        root.setPrev(root);

        for (FunctionPoint point : values)
            this.addNodeToTail().setPoint(point);
    }

    private FunctionNode getNodeByIndex(int index) {
        int steps;
        boolean isClockwise;
        FunctionNode node;

        if (index <= itemsCount - index - 1) {
            steps = index;
            node = root.getNext();
            isClockwise = true;
        }
        else {
            steps = itemsCount - index - 1;
            node = root.getPrev();
            isClockwise = false;
        }

        if (Math.abs(lastIndex - index) < steps) {
            steps = Math.abs(lastIndex - index);
            node = lastNode;
            isClockwise = lastIndex - index < 0;
        }

        for (int i = 0; i < steps; ++i) {
            node = isClockwise ? node.getNext() : node.getPrev();
        }

        lastNode = node;
        lastIndex = index;
        return node;
    }

    private FunctionNode addNodeToTail() {
        root.getPrev().setNext(new FunctionNode(root.getPrev(), root));
        root.setPrev(root.getPrev().getNext());

        ++itemsCount;
        lastNode = root.getPrev();
        lastIndex = itemsCount - 1;
        return root.getPrev();
    }

    public void deletePoint(int index) throws FunctionPointIndexOutOfBoundsException {
        if (index >= 0 && index <= itemsCount - 1) {
            if (itemsCount < 3) {
                throw new IllegalStateException("Кол-во точек в наборе менее трех. Удаление невозможно.");
            }
            FunctionNode node = deleteNodeByIndex(index);
            --itemsCount;
        }
        else {
            throw new FunctionPointIndexOutOfBoundsException("Выход за границы набора точек.");
        }
    }

    public void addPoint(FunctionPoint point) throws InappropriateFunctionPointException {
        if (root.getPrev().getPoint().getX() < point.getX()) {
            addNodeToTail().setPoint(point);
        }
        else {
            int i = 0;
            FunctionNode node = root.getNext();

            while (point.getX() >= node.getPoint().getX() && node != root) {
                node = node.getNext();
                ++i;
            }

            if (node.getPrev().getPoint().getX() == point.getX()) {
                throw new InappropriateFunctionPointException("Точка с данной абсциссой уже присутствует в интервале.");
            } else {
                addNodeByIndex(i).setPoint(point);
            }
        }
    }

    private FunctionNode addNodeByIndex(int index) {
        FunctionNode node = getNodeByIndex(index);

        node.getPrev().setNext(new FunctionNode(node.getPrev(), node));
        node.setPrev(node.getPrev().getNext());

        ++itemsCount;
        lastNode = node.getPrev();
        lastIndex = index;
        return node.getPrev();
    }

    private FunctionNode deleteNodeByIndex(int index) {
        FunctionNode node = getNodeByIndex(index);
        node.getPrev().setNext(node.getNext());
        node.getNext().setPrev(node.getPrev());
        node.setNext(null);
        node.setPrev(null);

        --itemsCount;

        return node;
    }

    public double getLeftDomainBorder() {
        return root.getNext().getPoint().getX();
    }

    public double getRightDomainBorder() {
        return root.getPrev().getPoint().getX();
    }

    public double getFunctionValue(double x) {
        if (x < getLeftDomainBorder() || x > getRightDomainBorder()) {
            return Double.NaN;
        }
        else {
            FunctionNode node = root.getNext();
            while(x > node.getPoint().getX()) {
                node = node.getNext();
            }

            if (x == node.getPoint().getX()) {
                return node.getPoint().getY();
            }
            else {
                return (node.getPoint().getY() - node.getPrev().getPoint().getY()) *
                        (x - node.getPrev().getPoint().getX()) /
                        (node.getPoint().getX() - node.getPrev().getPoint().getX()) +
                        node.getPrev().getPoint().getY();
            }
        }
    }

    public int getPointsCount() {
        return this.itemsCount;
    }

    public FunctionPoint getPoint(int index) throws FunctionPointIndexOutOfBoundsException {
        if (index >= 0 && index <= this.itemsCount - 1) {
            return getNodeByIndex(index).getPoint();
        }
        else {
            throw new FunctionPointIndexOutOfBoundsException("Выход за границы набора точек.");
        }
    }

    public void setPoint(int index, FunctionPoint point) throws FunctionPointIndexOutOfBoundsException, InappropriateFunctionPointException {
        if (index >= 0 && index <= this.itemsCount - 1) {
            FunctionNode node = getNodeByIndex(index);
            if (isPointInRange(index, point.getX(), node)) {
                node.setPoint(point);
            }
            else {
                throw new InappropriateFunctionPointException("Координата задаваемой точки лежит вне интервала, определяемого значениями соседних точек");
            }
        }
        else {
            throw new FunctionPointIndexOutOfBoundsException("Выход за границы набора точек.");
        }
    }

    private boolean isPointInRange(int index, double x, FunctionNode node) {
        return (index == 0 && x <= node.getNext().getPoint().getX() ||
                index == this.itemsCount - 1 && x >= node.getPrev().getPoint().getX() ||
                x <= node.getNext().getPoint().getX() && x >= node.getPrev().getPoint().getX());
    }

    public double getPointX(int index) throws FunctionPointIndexOutOfBoundsException {
        if (index >= 0 && index <= this.itemsCount - 1) {
            return getNodeByIndex(index).getPoint().getX();
        }
        else
        {
            throw new FunctionPointIndexOutOfBoundsException("Выход за границы набора точек.");
        }
    }

    public void setPointX(int index, double x) throws FunctionPointIndexOutOfBoundsException, InappropriateFunctionPointException {
        if (index >= 0 && index <= this.itemsCount - 1) {
            FunctionNode node = getNodeByIndex(index);
            if (isPointInRange(index, x, node)) {
                node.getPoint().setX(x);
            }
            else {
                throw new InappropriateFunctionPointException("Координата задаваемой точки лежит вне интервала, определяемого значениями соседних точек");
            }
        }
        else {
            throw new FunctionPointIndexOutOfBoundsException("Выход за границы набора точек.");
        }
    }

    public double getPointY(int index) throws FunctionPointIndexOutOfBoundsException {
        if (index >= 0 && index <= this.itemsCount - 1) {
            return getNodeByIndex(index).getPoint().getY();
        }
        else {
            throw new FunctionPointIndexOutOfBoundsException("Выход за границы набора точек.");
        }
    }

    public void setPointY(int index, double y) throws FunctionPointIndexOutOfBoundsException {
        if (index >= 0 && index <= this.itemsCount - 1) {
            getNodeByIndex(index).getPoint().setY(y);
        }
        else {
            throw new FunctionPointIndexOutOfBoundsException("Выход за границы набора точек.");
        }
    }

    public void printFunction() {
        System.out.print(this);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        FunctionNode node = root.getNext();
        while(node != root) {
            builder.append("f(" + node.getPoint().getX() + ") = " + node.getPoint().getY() + "\n");
            node = node.getNext();
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        TabulatedFunction tab = (TabulatedFunction)obj;
        if ((obj.getClass() == getClass() || obj.getClass() == LinkedListTabulatedFunction.class ) && tab.getPointsCount() == itemsCount) {
            for (int i = 0; i < itemsCount; ++i) {
                if (!getPoint(i).equals(tab.getPoint(i))) {
                    return false;
                }
            }
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = itemsCount;
        for (int i = 0; i < itemsCount; ++i) {
            result ^= getPoint(i).hashCode();
        }
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        FunctionPoint[] values = new FunctionPoint[itemsCount];
        for (int i = 0; i < itemsCount; ++i) {
            values[i] = (FunctionPoint)getPoint(i).clone();
        }
        return new ArrayTabulatedFunction(values);
    }

    @Override
    public Iterator<FunctionPoint> iterator() {
        return new Iterator<FunctionPoint>() {
            private FunctionNode node = root;

            @Override
            public boolean hasNext() {
                return node.getNext() != root;
            }

            @Override
            public FunctionPoint next() {
                if (node.getNext() == root) {
                    throw new NoSuchElementException();
                }
                FunctionNode result = node;
                node = node.getNext();
                return result.getPoint();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    class FunctionNode {
        private FunctionPoint point;
        private FunctionNode prev;
        private FunctionNode next;

        public FunctionNode() {
            point = new FunctionPoint();
            prev = null;
            next = null;
        }

        public FunctionNode(FunctionNode prev, FunctionNode next) {
            point = new FunctionPoint();
            this.prev = prev;
            this.next = next;
        }

        public FunctionNode(FunctionNode prev, FunctionNode next, FunctionPoint point) {
            this.point = point;
            this.prev = prev;
            this.next = next;
        }

        public void setPoint(FunctionPoint point) {
            this.point = point;
        }

        public void setNext(FunctionNode next) {
            this.next = next;
        }

        public void setPrev(FunctionNode prev) {
            this.prev = prev;
        }

        public FunctionPoint getPoint() {
            return point;
        }

        public FunctionNode getNext() {
            return next;
        }

        public FunctionNode getPrev() {
            return prev;
        }
    }
}