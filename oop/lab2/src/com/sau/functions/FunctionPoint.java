package com.sau.functions;

public class FunctionPoint implements Comparable<FunctionPoint>{
    private double x;
    private double y;

    public FunctionPoint() {
        this.x = 0;
        this.y = 0;
    }

    public FunctionPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public FunctionPoint(FunctionPoint point) {
        this.x = point.getX();
        this.y = point.getY();
    }

    public int compareTo(FunctionPoint point) {
        return Double.compare(this.getX(), point.getX());
    }

    public double getX() {
        return this.x;
    }

    public double setX(double x) {
        return this.x = x;
    }

    public double getY() {
        return this.y;
    }

    public double setY(double y) {
        return this.y = y;
    }
}
