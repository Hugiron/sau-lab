package com.sau;

import com.sau.functions.*;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        // f(x) = x^2
        TabulatedFunction function = new TabulatedFunction(0., 5., new double[]{0, 1, 4, 9, 16, 25});
        //function.addPoint(new FunctionPoint(-1, 1));
        function.deletePoint(2);
        function.setPointX(1, 0.5);
        function.setPointY(4, 40);
        double start = -1;
        double step = 0.25;
        for (int i = 0; i < 10; ++i) {
            double x = start + step * i;
            System.out.println("f(" + x + ") = " + function.getFunctionValue(x));
        }
        System.out.println();
    }
}
