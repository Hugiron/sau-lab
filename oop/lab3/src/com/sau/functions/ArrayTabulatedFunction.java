package com.sau.functions;

import com.sau.interfaces.TabulatedFunction;

public class ArrayTabulatedFunction implements TabulatedFunction{
    private SortedList<FunctionPoint> function;

    public ArrayTabulatedFunction(double leftX, double rightX, int pointsCount) throws IllegalArgumentException {
        if (leftX >= rightX || pointsCount < 2)
            throw new IllegalArgumentException();
        double step = (rightX - leftX) / (pointsCount - 1);
        this.function = new SortedList<>();
        for (int i = 0; i < pointsCount; ++i)
            this.function.add(new FunctionPoint(leftX + i * step, 0));
        this.function.get(this.function.count() - 1).setX(rightX);
    }

    public ArrayTabulatedFunction(double leftX, double rightX, double[] values) throws IllegalArgumentException {
        if (leftX >= rightX || values.length < 2)
            throw new IllegalArgumentException();
        double step = (rightX - leftX) / (values.length - 1);
        this.function = new SortedList<>();
        for (int i = 0; i < values.length; ++i)
            this.function.add(new FunctionPoint(leftX + i * step, values[i]));
        this.function.get(this.function.count() - 1).setX(rightX);
    }

    public double getLeftDomainBorder() {
        return this.function.get(0).getX();
    }

    public double getRightDomainBorder() {
        return this.function.get(this.function.count() - 1).getX();
    }

    public double getFunctionValue(double x) {
        if (x < this.getLeftDomainBorder() || x > this.getRightDomainBorder())
            return Double.NaN;
        int rightIndex = this.function.binarySearch(new FunctionPoint(x, 0));
        int leftIndex = rightIndex - 1;
        return this.getFunctionValue(x, this.function.get(leftIndex), this.function.get(rightIndex));
    }

    private double getFunctionValue(double x, FunctionPoint first, FunctionPoint second) {
        return (second.getY() - first.getY()) * (x - first.getX()) / (second.getX() - first.getX()) + first.getY();
    }

    public int getPointsCount() {
        return this.function.count();
    }

    public FunctionPoint getPoint(int index) throws FunctionPointIndexOutOfBoundsException {
        if (index < 0 || index >= this.function.count())
            throw new FunctionPointIndexOutOfBoundsException();
        return this.function.get(index);
    }

    public void setPoint(int index, FunctionPoint point) throws FunctionPointIndexOutOfBoundsException,
            InappropriateFunctionPointException {
        if (index < 0 || index >= this.function.count())
            throw new FunctionPointIndexOutOfBoundsException();
        if (this.function.get(index - 1).getX() >= point.getX() && index > 0
                || this.function.get(index + 1).getX() <= point.getX() && index < this.function.count() - 1)
            throw new InappropriateFunctionPointException();
        this.function.set(index, point);
    }

    public double getPointX(int index) throws FunctionPointIndexOutOfBoundsException {
        return this.getPoint(index).getX();
    }

    public void setPointX(int index, double x) throws FunctionPointIndexOutOfBoundsException,
            InappropriateFunctionPointException {
        double y = this.getFunctionValue(x);
        this.setPoint(index, new FunctionPoint(x, y));
    }

    public double getPointY(int index) throws FunctionPointIndexOutOfBoundsException {
        return this.getPoint(index).getY();
    }

    public void setPointY(int index, double y) throws FunctionPointIndexOutOfBoundsException {
        this.getPoint(index).setY(y);
    }

    public void deletePoint(int index) throws FunctionPointIndexOutOfBoundsException, IllegalStateException{
        if (index < 0 || index >= this.function.count())
            throw new FunctionPointIndexOutOfBoundsException();
        if (this.function.count() < 3)
            throw new IllegalStateException();
        this.function.remove(index);
    }

    public void addPoint(FunctionPoint point) throws InappropriateFunctionPointException{
        if (this.function.getByValue(point).getX() == point.getX())
            throw new InappropriateFunctionPointException();
        this.function.add(point);
    }

    public void printFunction() {
        System.out.print(this);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < this.function.count(); ++i)
            builder.append("f(" + this.function.get(i).getX() + ") = " + this.function.get(i).getY() + "\n");
        return builder.toString();
    }
}
