package com.sau;

import com.sau.functions.*;
import com.sau.interfaces.*;

public class Main {
    public static void main(String[] args) throws FunctionPointIndexOutOfBoundsException, InappropriateFunctionPointException {
        double[] a = {0, 4, 5, 7};
        TabulatedFunction f = new LinkedListTabulatedFunction(0.0, 6.0, a);
        f.printFunction();
        System.out.println();

        System.out.println("Кол-во точек: " + f.getPointsCount());
        System.out.println();
        f.setPoint(0, new FunctionPoint(-1, 5));

        System.out.println();
        f.printFunction();
        System.out.println();

        f.addPoint(new FunctionPoint(7, 1));
        f.addPoint(new FunctionPoint(5, 3));

        System.out.println();
        f.printFunction();
        System.out.println();

        System.out.println("x = 4.5, y = " + f.getFunctionValue(4.5));

        System.out.println("Кол-во точек = " + f.getPointsCount());

        f.setPointX(2, 4.1);
        f.setPointY(2, 5.1);

        System.out.println();
        f.printFunction();
        System.out.println();

        f.deletePoint(4);

        System.out.println();
        f.printFunction();
        System.out.println();
    }
}
