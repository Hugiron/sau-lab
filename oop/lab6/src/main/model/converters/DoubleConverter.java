package main.model.converters;

import javafx.util.StringConverter;

public class DoubleConverter extends StringConverter<Double> {
    @Override
    public String toString(Double object) {
        if (object == null)
            return "NaN";
        return object.toString();
    }

    @Override
    public Double fromString(String string) {
        try {
            return Double.parseDouble(string);
        } catch (Exception ex) {
            return null;
        }
    }
}
