package main.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import main.model.functions.ArrayTabulatedFunction;
import main.model.functions.FunctionPoint;
import main.model.interfaces.TabulatedFunction;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Configuration {
    private TabulatedFunction currentFunction;
    private TableView<FunctionPoint> table;
    private String filepath;

    private static Configuration ourInstance = new Configuration();

    public static Configuration getInstance() {
        return ourInstance;
    }

    private Configuration() {
    }

    public void renderFunction() {
        if (this.currentFunction == null || this.table == null)
            return;
        ObservableList<FunctionPoint> functionData = FXCollections.observableArrayList();
        for (int i = 0; i < this.currentFunction.getPointsCount(); ++i)
            functionData.add(this.currentFunction.getPoint(i));
        table.setItems(functionData);
    }

    public TabulatedFunction getCurrentFunction() {
        return this.currentFunction;
    }

    public void setTable(TableView<FunctionPoint> table) {
        this.table = table;
    }

    public void setCurrentFunction(TabulatedFunction currentFunction) {
        this.currentFunction = currentFunction;
    }

    public String getFilepath() {
        return this.filepath;
    }

    public void setFilepath(String filepath) {
        if (filepath == null || filepath.equals(this.filepath)) {
            if (filepath == null)
                this.filepath = filepath;
            return;
        }
        try {
            FileInputStream inputStream = new FileInputStream(filepath);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            this.currentFunction = (TabulatedFunction)objectInputStream.readObject();
            objectInputStream.close();
            inputStream.close();
            this.filepath = filepath;
            this.renderFunction();
        } catch (Exception ex) {
            Alert message = new Alert(Alert.AlertType.ERROR);
            message.setContentText("Не удалось открыть функцию");
            message.setTitle("Ошибка чтения");
            message.show();
        }
    }

    public void save(String path) {
        try {
            FileOutputStream outputStream = new FileOutputStream(path);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(this.currentFunction);
            objectOutputStream.flush();
            objectOutputStream.close();
            outputStream.close();
        } catch (Exception ex) {
            Alert message = new Alert(Alert.AlertType.ERROR);
            message.setContentText("Не удалось сохранить функцию");
            message.setTitle("Ошибка записи");
            message.show();
        }
    }

    public boolean isEdited() {
        try {
            FileInputStream inputStream = new FileInputStream(filepath);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            TabulatedFunction function = (TabulatedFunction)objectInputStream.readObject();
            objectInputStream.close();
            inputStream.close();
            return !this.currentFunction.equals(currentFunction);
        } catch (Exception ex) {
            return true;
        }
    }
}
