package main.model.functions.meta;

import main.model.interfaces.Function;

public class Shift implements Function {
    private Function function;
    private double oX;
    private double oY;

    public Shift(Function function, double oX, double oY) {
        this.function = function;
        this.oX = oX;
        this.oY = oY;
    }

    @Override
    public double getLeftDomainBorder() {
        return function.getLeftDomainBorder() + oX;
    }

    @Override
    public double getRightDomainBorder() {
        return function.getRightDomainBorder() + oX;
    }

    @Override
    public double getFunctionValue(double x) {
        if (x < getLeftDomainBorder() || x > getRightDomainBorder())
            return Double.NaN;
        return function.getFunctionValue(x) + oY;
    }
}
