package main.model.functions.basic;

public class Cos extends TrigonometricFunction {
    @Override
    public double getFunctionValue(double x) {
        if (x < getLeftDomainBorder() || x > getRightDomainBorder())
            return Double.NaN;
        return Math.cos(x);
    }
}
