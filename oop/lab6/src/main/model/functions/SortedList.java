package main.model.functions;

import java.io.Serializable;

public class SortedList<T extends Comparable<T>> implements Serializable {
    private static final int MAX_SIZE_LIST = Integer.MAX_VALUE;
    private static final int DEFAULT_CAPACITY = 10;

    private Object[] buffer;
    private int size;

    public SortedList() {
        this.buffer = new Object[DEFAULT_CAPACITY];
        this.size = 0;
    }

    public T get(int index) {
        this.rangeCheck(index);
        @SuppressWarnings("unchecked")
        final T item = (T)this.buffer[index];
        return item;
    }

    public void set(int index, T value) {
        this.rangeCheck(index);
        if (index == 0 && this.get(index + 1).compareTo(value) > 0 ||
                index == this.count() - 1 && this.get(index - 1).compareTo(value) < 0 ||
                index > 0 && index < this.count() - 1 && this.get(index + 1).compareTo(value) > 0
                        && this.get(index - 1).compareTo(value) < 0)
            this.buffer[index] = value;
    }

    public T getByValue(T value) {
        int index = binarySearch(value);
        if (index < this.size && value.compareTo(this.get(index)) == 0)
            return this.get(index);
        return null;
    }

    public void add(T value) {
        if (this.size == MAX_SIZE_LIST)
            throw new OutOfMemoryError();
        int index = this.count() > 0 ? binarySearch(value) : 0;
        this.size++;
        encrease();
        for (int i = this.size - 1; i > index; --i)
            this.buffer[i] = this.buffer[i - 1];
        this.buffer[index] = value;
    }

    public void remove(int index) {
        rangeCheck(index);
        this.size--;
        for (int i = index; i < this.size; ++i)
            this.buffer[i] = this.buffer[i + 1];
        decrease();
    }

    public void removeByValue(T value) {
        int index = binarySearch(value);
        if (index < this.size && value.compareTo(this.get(index)) == 0)
            this.remove(index);
    }

    public int count() {
        return this.size;
    }

    public int binarySearch(T value) {
        int left = 0, right = this.size;
        while (left < right) {
            int mid = (left + right) / 2;
            if (value.compareTo(this.get(mid)) < 0)
                right = mid;
            else
                left = mid + 1;
        }
        return left;
    }

    private void encrease() {
        if (2 * this.size <= buffer.length)
            return;
        int size = 2 * buffer.length < buffer.length ? MAX_SIZE_LIST : 2 * buffer.length;
        Object[] temp = new Object[size];
        System.arraycopy(this.buffer, 0, temp, 0, this.size);
        this.buffer = temp;
    }

    private void decrease() {
        if (4 * this.size > buffer.length)
            return;
        int size = Math.max(DEFAULT_CAPACITY, buffer.length / 2);
        Object[] temp = new Object[size];
        System.arraycopy(this.buffer, 0, temp, 0, this.size);
        this.buffer = temp;
    }

    private void rangeCheck(int index) {
        if (index < 0 || index >= this.size)
            throw new IndexOutOfBoundsException(this.outOfBoundsMessage(index));
    }

    private String outOfBoundsMessage(int index) {
        return "Index: " + index + ", Size: " + this.size;
    }
}
