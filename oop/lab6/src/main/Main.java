package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import main.controller.MainController;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/main.fxml"));
        loader.setController(new MainController());
        Parent root = loader.load();
        primaryStage.setTitle("Tabulated Functions");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.setOnCloseRequest((event) -> ((MainController)loader.getController()).exit());
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
