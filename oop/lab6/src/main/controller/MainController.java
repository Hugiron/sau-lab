package main.controller;

import editor.ModuleClassLoader;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.model.Configuration;
import main.model.converters.DoubleConverter;
import main.model.functions.ArrayTabulatedFunction;
import main.model.functions.FunctionPoint;
import main.model.functions.InappropriateFunctionPointException;
import main.model.interfaces.Function;

import java.io.File;
import java.io.IOException;

public class MainController {
    @FXML
    private MenuItem createFunctionButton;
    @FXML
    private MenuItem openFunctionButton;
    @FXML
    private MenuItem saveFunctionButton;
    @FXML
    private MenuItem saveAsFunctionButton;
    @FXML
    private MenuItem exitButton;

    @FXML
    private MenuItem loadFunctionButton;
    @FXML
    private MenuItem drawFunctionButton;

    @FXML
    private TableView<FunctionPoint> functionTable;

    @FXML
    private TableColumn<FunctionPoint, Double> argumentColumn;
    @FXML
    private TableColumn<FunctionPoint, Double> functionColumn;

    @FXML
    private TextField fieldPointX;
    @FXML
    private TextField fieldPointY;

    @FXML
    private Button addPointButton;
    @FXML
    private Button deletePointButton;

    @FXML
    public void initialize() {
        Configuration config = Configuration.getInstance();
        config.setTable(this.functionTable);
        config.setCurrentFunction(new ArrayTabulatedFunction(0, 10, 11));
        config.renderFunction();

        this.argumentColumn.setCellValueFactory(new PropertyValueFactory("x"));
        this.functionColumn.setCellValueFactory(new PropertyValueFactory("y"));

        this.argumentColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleConverter()));
        this.functionColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleConverter()));

        this.argumentColumn.setOnEditCommit((value) -> {
            try {
                int index = value.getTablePosition().getRow();
                if (!value.getNewValue().equals(value.getOldValue())) {
                    FunctionPoint point = new FunctionPoint(value.getNewValue(), config.getCurrentFunction().getPointY(index));
                    try {
                        config.getCurrentFunction().addPoint(point);
                    } catch (InappropriateFunctionPointException ex) {}
                    config.getCurrentFunction().deletePoint(index + (value.getNewValue() > value.getOldValue() ? 0 : 1));
                    this.functionTable.getItems().clear();
                    config.renderFunction();
                }
            } catch (Exception ex) {}
        });
        this.functionColumn.setOnEditCommit((value) -> {
            value.getRowValue().setY(value.getNewValue());
        });

        this.createFunctionButton.setOnAction((event) -> {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/settings.fxml"));
                loader.setController(new SettingsController());
                Parent parent = loader.load();
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setTitle("Function paramerers");
                stage.setScene(new Scene(parent));
                stage.requestFocus();
                stage.show();
            }
            catch (IOException ioException) {
                System.out.println(ioException.getMessage());
            }
        });

        this.openFunctionButton.setOnAction((event) -> {
            FileChooser chooser = new FileChooser();
            File file = chooser.showOpenDialog(this.openFunctionButton.getParentPopup().getOwnerWindow());
            if (file != null && file.exists())
                config.setFilepath(file.getAbsolutePath());
        });

        this.saveFunctionButton.setOnAction((event) -> {
            if (config.getFilepath() == null || !new File(config.getFilepath()).exists())
                saveFunction();
            else
                config.save(config.getFilepath());
        });

        this.saveAsFunctionButton.setOnAction((event) -> this.saveFunction());

        this.exitButton.setOnAction((event) -> exit());

        this.addPointButton.setOnAction((event) -> {
            double x = Double.parseDouble(this.fieldPointX.getText());
            double y = Double.parseDouble(this.fieldPointY.getText());
            try {
                config.getCurrentFunction().addPoint(new FunctionPoint(x, y));
            } catch (InappropriateFunctionPointException ex) {}
            this.functionTable.getItems().clear();
            config.renderFunction();
            this.fieldPointX.setText("");
            this.fieldPointY.setText("");
        });

        this.deletePointButton.setOnAction((event) -> {
            try {
                int index = this.functionTable.getFocusModel().getFocusedIndex();
                config.getCurrentFunction().deletePoint(index);
                this.functionTable.getItems().clear();
                config.renderFunction();
            } catch (Exception ex) {}
        });

        this.loadFunctionButton.setOnAction((event) -> {
            FileChooser chooser = new FileChooser();
            File file = chooser.showOpenDialog(this.loadFunctionButton.getParentPopup().getOwnerWindow());
            if (file == null)
                return;
            try {
                Function function = (Function)new ModuleClassLoader().loadClassFromFile(file.getAbsolutePath()).newInstance();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/settings.fxml"));
                loader.setController(new SettingsController(function));
                Parent parent = loader.load();
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setTitle("Function paramerers");
                stage.setScene(new Scene(parent));
                stage.requestFocus();
                stage.show();
            } catch (Throwable ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Не удалось загрузить объект. Возможно, файл поврежден.");
                alert.show();
            }
        });

        this.fieldPointX.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
                return;
            try {
                Double.parseDouble(this.fieldPointX.getText());
            } catch (Exception ex) {
                this.fieldPointX.setText("0");
            }
        });

        this.fieldPointY.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
                return;
            try {
                Double.parseDouble(this.fieldPointY.getText());
            } catch (Exception ex) {
                this.fieldPointY.setText("0");
            }
        });

        this.drawFunctionButton.setOnAction((event) -> {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/graph.fxml"));
                loader.setController(new GraphController());
                Parent parent = loader.load();
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setTitle("Chart for current function");
                stage.setScene(new Scene(parent));
                stage.requestFocus();
                stage.show();
            } catch (Exception ex) {}
        });
    }

    public void saveFunction() {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(this.saveFunctionButton.getParentPopup().getOwnerWindow());
        if (file == null)
            return;
        Configuration.getInstance().save(file.getAbsolutePath());
    }

    public void exit() {
        if (Configuration.getInstance().getCurrentFunction() != null)
            this.autosave();
        System.exit(0);
    }

    public void autosave() {
        Configuration config = Configuration.getInstance();
        boolean isEdited = config.isEdited();
        if (config.getFilepath() == null && config.getCurrentFunction().getPointsCount() > 0 || isEdited) {
            Alert message = new Alert(Alert.AlertType.CONFIRMATION);
            message.setContentText("Сохранить текущую функцию для дальнейшего использования?");
            message.showAndWait();
            if (message.getResult() == ButtonType.CANCEL)
                return;
            if (config.getFilepath() == null || !new File(config.getFilepath()).exists())
                saveFunction();
            else
                config.save(config.getFilepath());
        }
    }
}
