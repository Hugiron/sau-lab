package main.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import main.model.Configuration;
import main.model.functions.ArrayTabulatedFunction;
import main.model.functions.TabulatedFunctions;
import main.model.interfaces.Function;
import main.model.interfaces.TabulatedFunction;

public class SettingsController {
    @FXML
    private TextField leftData;
    @FXML
    private TextField rightData;
    @FXML
    private Spinner<Integer> countData;

    @FXML
    private Button okButton;
    @FXML
    private Button cancelButton;

    private Function currentFunction;

    public SettingsController() {}

    public SettingsController(Function function) {
        this.currentFunction = function;
    }

    @FXML
    public void initialize() {
        this.leftData.setText("0");
        this.rightData.setText("10");

        this.leftData.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
                return;
            try {
                Double.parseDouble(this.leftData.getText());
            } catch (Exception ex) {
                this.leftData.setText("0");
            }
        });

        this.rightData.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
                return;
            try {
                Double.parseDouble(this.rightData.getText());
            } catch (Exception ex) {
                this.rightData.setText("0");
            }
        });

        this.okButton.setOnAction((event) -> {
            double left = Double.parseDouble(this.leftData.getText());
            double right = Double.parseDouble(this.rightData.getText());
            if (left >= right) {
                Alert message = new Alert(Alert.AlertType.ERROR);
                message.setContentText("Значение правой границы не может быть меньше значения левой границы");
                message.setTitle("Ошибка");
                message.show();
                return;
            }
            int count = this.countData.getValue();
            TabulatedFunction tabulate = this.currentFunction == null ? new ArrayTabulatedFunction(left, right, count) :
                    TabulatedFunctions.tabulate(this.currentFunction, left, right, count);
            Configuration.getInstance().setCurrentFunction(tabulate);
            Configuration.getInstance().renderFunction();
            Configuration.getInstance().setFilepath(null);
            ((Stage)this.okButton.getScene().getWindow()).close();
        });

        this.cancelButton.setOnAction((event) -> {
            ((Stage)this.cancelButton.getScene().getWindow()).close();
        });
    }
}
