package main.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import main.model.Configuration;

public class GraphController {
    @FXML
    private LineChart<Double, Double> functionChart;

    @FXML
    public void initialize() {
        ObservableList<XYChart.Series<Double, Double>> chart = FXCollections.observableArrayList();
        XYChart.Series<Double, Double> data = new XYChart.Series<>();
        Configuration config = Configuration.getInstance();
        for (int i = 0; i < config.getCurrentFunction().getPointsCount(); ++i)
            data.getData().add(new XYChart.Data<>(config.getCurrentFunction().getPointX(i), config.getCurrentFunction().getPointY(i)));
        chart.add(data);
        this.functionChart.setCreateSymbols(false);
        this.functionChart.setData(chart);
        this.functionChart.autosize();
    }
}
