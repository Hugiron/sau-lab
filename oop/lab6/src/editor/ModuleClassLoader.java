package editor;

import java.io.FileInputStream;
import java.io.IOException;

public class ModuleClassLoader extends ClassLoader {
    public Class loadClassFromFile(String filename) throws IOException {
        byte[] b = this.loadClassData(filename);
        return this.defineClass((String)null, b, 0, b.length);
    }

    private byte[] loadClassData(String filename) throws IOException {
        FileInputStream inputStream = new FileInputStream(filename);
        Throwable except = null;
        byte[] fileContent;
        try {
            fileContent = new byte[inputStream.available()];
            inputStream.read(fileContent);
        } catch (Throwable exception) {
            except = exception;
            throw exception;
        } finally {
            if(inputStream != null) {
                if(except != null) {
                    try {
                        inputStream.close();
                    } catch (Throwable var12) {
                        except.addSuppressed(var12);
                    }
                } else
                    inputStream.close();
            }

        }
        return fileContent;
    }
}

