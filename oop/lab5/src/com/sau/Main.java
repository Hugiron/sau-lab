package com.sau;

import com.sau.functions.*;
import com.sau.interfaces.*;
import com.sau.functions.basic.*;

import java.io.*;

public class Main {
    public static void main(String[] args)
            throws FunctionPointIndexOutOfBoundsException, InappropriateFunctionPointException, IOException, ClassNotFoundException {
        System.out.println("Trigonometric functions from Math:");
        Sin sin = new Sin();
        for (double x = 0; x < 2 * Math.PI; x += 0.1)
            System.out.println("sin(" + Math.round(x * 10d) / 10d + ") = " + Math.round(sin.getFunctionValue(x) * 100d) / 100d);
        Cos cos = new Cos();
        for (double x = 0; x < 2 * Math.PI; x += 0.1)
            System.out.println("cos(" + Math.round(x * 10d) / 10d + ") = " + Math.round(cos.getFunctionValue(x) * 100d) / 100d);

        System.out.println("\nTrigonometric tabulated functions:");
        Function tabulateSin = TabulatedFunctions.tabulate(new Sin(), 0, 2 * Math.PI, 10);
        for (double x = 0; x < 2 * Math.PI; x += 0.1)
            System.out.println("sin(" + Math.round(x * 10d) / 10d + ") = " + Math.round(tabulateSin.getFunctionValue(x) * 100d) / 100d);
        Function tabulateCos = TabulatedFunctions.tabulate(new Cos(), 0, 2 * Math.PI, 10);
        for (double x = 0; x < 2 * Math.PI; x += 0.1)
            System.out.println("cos(" + Math.round(x * 10d) / 10d + ") = " + Math.round(tabulateCos.getFunctionValue(x) * 100d) / 100d);

        System.out.println("\nSum of functions of the squares");
        Function function = Functions.sum(Functions.power(new Sin(), 2), Functions.power(new Cos(), 2));
        for (double x = 0; x < 2 * Math.PI; x += 0.1)
            System.out.println("sum(" + Math.round(x * 10d) / 10d + ") = " + Math.round(function.getFunctionValue(x) * 100d) / 100d);

        System.out.println("\nExp tabulated function:");
        TabulatedFunction tabulateExp = TabulatedFunctions.tabulate(new Exp(), 0, 10, 11);
        FileWriter expWriter = new FileWriter("exp.dump");
        TabulatedFunctions.writeTabulatedFunction(tabulateExp, expWriter);
        expWriter.close();
        FileReader expReader = new FileReader("exp.dump");
        TabulatedFunction dumpTabulateExp = TabulatedFunctions.readTabulatedFunction(expReader);
        expReader.close();
        for (int i = 0; i < 11; ++i)
            System.out.println("source(" + i + ") = " + Math.round(100d * tabulateExp.getPoint(i).getY()) / 100d +
                    " | dump(" + i + ") = " + Math.round(100d * dumpTabulateExp.getPoint(i).getY()) / 100d);

        System.out.println("\nLog tabulated function:");
        TabulatedFunction tabulateLog = TabulatedFunctions.tabulate(new Log(Math.E), 0, 10, 11);
        FileWriter logWriter = new FileWriter("log.dump");
        TabulatedFunctions.writeTabulatedFunction(tabulateLog, logWriter);
        logWriter.close();
        FileReader logReader = new FileReader("log.dump");
        TabulatedFunction dumpTabulateLog = TabulatedFunctions.readTabulatedFunction(logReader);
        logReader.close();
        for (int i = 0; i < 11; ++i)
            System.out.println("source(" + i + ") = " + Math.round(100d * tabulateLog.getPoint(i).getY()) / 100d +
                    " | dump(" + i + ") = " + Math.round(100d * dumpTabulateLog.getPoint(i).getY()) / 100d);

        System.out.println("\nTest of serialization:");
        TabulatedFunction compose = TabulatedFunctions.tabulate(Functions.composition(new Log(Math.E), new Exp()), 0, 10, 11);
        FileOutputStream composeWriter = new FileOutputStream("compose.dump");
        ObjectOutputStream oos = new ObjectOutputStream(composeWriter);
        oos.writeObject(compose);
        oos.flush();
        oos.close();
        composeWriter.close();
        FileInputStream composeReader = new FileInputStream("compose.dump");
        ObjectInputStream ois = new ObjectInputStream(composeReader);
        TabulatedFunction dumpCompose = (TabulatedFunction)ois.readObject();
        ois.close();
        composeReader.close();
        for (int i = 0; i < 11; ++i)
            System.out.println("source(" + i + ") = " + Math.round(100d * compose.getPoint(i).getY()) / 100d +
                    " | dump(" + i + ") = " + Math.round(100d * dumpCompose.getPoint(i).getY()) / 100d);
    }
}
