package com.sau.functions.meta;

import com.sau.interfaces.Function;

public class Scale implements Function{
    private Function function;
    private double oX;
    private double oY;

    public Scale(Function function, double oX, double oY) {
        this.function = function;
        this.oX = oX == 0 ? 1 : oX;
        this.oY = oY == 0 ? 1 : oY;
    }

    @Override
    public double getLeftDomainBorder() {
        return oX >= 0 ? function.getLeftDomainBorder() * oX : function.getLeftDomainBorder() / oX;
    }

    @Override
    public double getRightDomainBorder() {
        return oX >= 0 ? function.getLeftDomainBorder() * oX : function.getLeftDomainBorder() / oX;
    }

    @Override
    public double getFunctionValue(double x) {
        if (x < getLeftDomainBorder() || x > getRightDomainBorder())
            return Double.NaN;
        return oY >= 0 ? function.getFunctionValue(x) * oY : function.getFunctionValue(x) / oY;
    }
}
