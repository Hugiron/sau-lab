package com.sau.functions;

import com.sau.interfaces.Function;
import com.sau.interfaces.TabulatedFunction;

import java.io.*;

public final class TabulatedFunctions {
    public static TabulatedFunction tabulate(Function function, double leftX, double rightX, int pointsCount) throws IllegalArgumentException {
        if (function.getLeftDomainBorder() > leftX || function.getRightDomainBorder() < rightX )
            throw new IllegalArgumentException("Выход за границы табуляции.");
        double step = (rightX - leftX) / (pointsCount - 1);
        double[] points = new double[pointsCount];

        for (int i = 0; i < pointsCount; ++i)
            points[i] = function.getFunctionValue(leftX + step * i);

        return new ArrayTabulatedFunction(leftX, rightX, points);
    }

    public static void outputTabulatedFunction(TabulatedFunction function, OutputStream out) throws IOException {
        DataOutputStream dos = new DataOutputStream(out);
        dos.writeInt(function.getPointsCount());
        for (int i = 0; i < function.getPointsCount(); ++i) {
            dos.writeDouble(function.getPoint(i).getX());
            dos.writeDouble(function.getPoint(i).getY());
        }
    }

    public static TabulatedFunction inputTabulatedFunction(InputStream in) throws IOException {
        DataInputStream is = new DataInputStream(in);
        int itemsCount = is.readInt();
        FunctionPoint[] date = new FunctionPoint[itemsCount];
        double x, y;
        for (int i = 0; i < itemsCount; ++i) {
            x = is.readDouble();
            y = is.readDouble();
            date[i] = new FunctionPoint(x, y);
        }

        return new ArrayTabulatedFunction(date);
    }

    public static void writeTabulatedFunction(TabulatedFunction function, Writer out) {
        PrintWriter os = new PrintWriter(out);
        os.println(function.getPointsCount());
        for (int i = 0; i < function.getPointsCount(); ++i) {
            os.println(String.valueOf(function.getPoint(i).getX()));
            os.println(String.valueOf(function.getPoint(i).getY()));
        }
    }

    public static TabulatedFunction readTabulatedFunction(Reader in) throws IOException {
        StreamTokenizer st = new StreamTokenizer(in);
        int token = st.nextToken();
        int itemsCount = (int)st.nval;
        FunctionPoint[] date = new FunctionPoint[itemsCount];
        double x, y;
        for (int i = 0; i < itemsCount; ++i) {
            st.nextToken();
            x = st.nval;
            st.nextToken();
            y = st.nval;
            date[i] = new FunctionPoint(x, y);
        }

        return new ArrayTabulatedFunction(date);
    }
}
